export default {
    // Target (https://go.nuxtjs.dev/config-target)
    target: 'static',

    // Global page headers (https://go.nuxtjs.dev/config-head)
    head: {
        title: 'Flaggen Quiz',
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
            },
            {
                name: 'theme-color',
                content: '#09090c',
            },
            {
                name: 'msapplication-TileColor',
                content: '#da532c',
            },
            {
                hid: 'description',
                name: 'description',
                content: 'Spiele mit deinem Twitch Chat das Flaggen Quiz.',
            },
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: 'apple-touch-icon',
                href: '/apple-touch-icon.png',
                size: '180x180',
            },
            {
                rel: 'icon',
                type: 'image/png',
                href: '/favicon-32x32.png',
                size: '32x32',
            },
            {
                rel: 'icon',
                type: 'image/png',
                href: '/favicon-16x16.png',
                size: '16x16',
            },
            { rel: 'manifest', href: '/site.webmanifest' },
            {
                rel: 'mask-icon',
                href: '/safari-pinned-tab.svg',
                color: '#09090c',
            },
            {
                href:
                    'https://fonts.googleapis.com/css2?family=Inter:wght@400;500;800;900&display=swap',
                rel: 'stylesheet',
            },
        ],
    },

    // Global CSS (https://go.nuxtjs.dev/config-css)
    css: [],

    // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
    plugins: [],

    // Auto import components (https://go.nuxtjs.dev/config-components)
    components: true,

    // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
    buildModules: [
        // https://go.nuxtjs.dev/eslint
        '@nuxtjs/eslint-module',
    ],

    // Modules (https://go.nuxtjs.dev/config-modules)
    modules: [
        // https://go.nuxtjs.dev/pwa
        '@nuxtjs/pwa',
    ],

    // Build Configuration (https://go.nuxtjs.dev/config-build)
    build: {},
}
